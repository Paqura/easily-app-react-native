import React from 'react';
import { AppRegistry, Text, View } from 'react-native';
import Header from './src/components/Header';
import ProductList from './src/components/ProductList';

const App = () => {
  return (
    <View style={{flex: 1}}>     
      <Header headerText="Products"/>
      <ProductList />
    </View>
  )
};

AppRegistry.registerComponent('albums', () => App);