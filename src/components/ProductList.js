import React, { Component } from 'react';
import {ScrollView,  ProgressBarAndroid} from 'react-native';
import axios from 'axios';
import ProductItem from './ProductItem';
import {api} from '../helpers';

class ProductList extends Component {
  state = {
    products: []
  }
  componentDidMount() {
    axios.get(api)
      .then(res => {
        this.setState({
          products: res.data.slice(0, 20)
        })
      })
  }

  renderProducts = (product) => {
    return (
      <ProductItem 
        key={product.title} 
        product={product} 
      />
    )
  }

  render() {
    const {products} = this.state;
    return (
      <ScrollView>
        { 
          products.length ? products.map(it => this.renderProducts(it)) 
          : <ProgressBarAndroid style={styles.progressStyle}/>
        }
      </ScrollView>
    )
  }
}

const styles = {
  progressStyle: {
    top: 200
  }
}

export default ProductList;
