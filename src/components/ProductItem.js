import React from 'react';
import {View, Text, Image} from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

const ProductItem = ({product}) => {
  return (
    <Card key={product.id}>
      <CardSection style={styles.headerStyles}>
        <View>
          <Image
            style={styles.imageStyles}
            source={{
              uri: product.thumbnailUrl
            }}
          />
          <Text style={styles.textStyle}>
            {product.title.slice(0, 5)}
          </Text>
        </View>     
      </CardSection>
      <CardSection>
        <Image
          style={styles.bigImageStyle}
          source={{
            uri: product.url
          }}
        />
      </CardSection>
      <CardSection>
        <Button/>
      </CardSection>
    </Card>
  )
}

const styles = {
  imageStyles: {
    width: 50,
    height: 50
  },
  headerStyles: {
    marginLeft: 8,
    marginRight: 8,
    flexDirection: 'row'
  },
  bigImageStyle: {
    height: 300,
    flex: 1,
    width: null
  }
}

export default ProductItem;